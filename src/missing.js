const fs = require('fs')
const path = require('path')
const { default: scraper } = require('anime-scrape')
const { EventEmitter } = require('events')

const MAX_CHECK = 6

function checkCorrupt (episodePath) {
  const { size } = fs.statSync(episodePath)
  if (Math.floor(size / 1024 / 1024) <= 3) {
    fs.unlinkSync(episodePath)
    return true
  }
  return false
}

function getMissing (animePath) {
  return new Promise(async resolve => {
    const emitter = new EventEmitter()
    const animeList = (await scraper.getList()).map(({ link }) => link)
    const animeLatest = await scraper.getLatest()

    const animeDirectory = fs.readdirSync(animePath).filter(a => animeList.includes(a))

    const checkedAnime = []
    let inQueue = 0

    if (!animeDirectory.length) return

    emitter.on('check', anime => {
      const { link, episodes } = anime
      const animePathEpisodes = fs
        .readdirSync(path.join(animePath, link))
        .filter(e => !checkCorrupt(path.join(animePath, link, e)))
        .map(e => e.split(' ')[1].split('.')[0])
      const missingEpisodes = episodes.filter(e => !animePathEpisodes.includes(e))
      emitter.emit('checked', Object.assign({}, anime, { episodes: missingEpisodes }))
    })

    emitter.on('details', async anime => {
      const details = await scraper.getDetail(anime)

      const foundLatest = animeLatest
        .filter(({ link }) => link === details.link)
        .map(anime => anime.episode)

      if (foundLatest.length) {
        details.episodes.push(...foundLatest.filter(e => !details.episodes.includes(e)))
      }

      emitter.emit('check', details)
    })

    emitter.on('checked', anime => {
      checkedAnime.push(anime)

      // eslint-disable-next-line
      inQueue--;

      if (inQueue < MAX_CHECK && animeDirectory.length && inQueue !== 0) {
        emitter.emit('details', animeDirectory.pop())
        inQueue++
        return
      }

      emitter.emit('end')
    })

    emitter.on('end', () => {
      resolve(checkedAnime)
    })

    // start
    for (let i = 0; i < MAX_CHECK; i++) {
      emitter.emit('details', animeDirectory.pop())
    }
  })
}

module.exports = { getMissing, checkCorrupt }
