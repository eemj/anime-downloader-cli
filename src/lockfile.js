const fs = require('fs')
const path = require('path')
const { log } = require('./helpers')

const LOCKFILE = path.join(__dirname, 'anime-downloader.lock')

if (exists()) {
  log.error(`A download process is currently on the go`)
  process.exit(1)
}

function exists () {
  return fs.existsSync(LOCKFILE)
}

function lock () {
  if (!exists()) {
    fs.writeFileSync(LOCKFILE, '')
  }
}

function unlock () {
  if (exists()) {
    fs.unlinkSync(LOCKFILE)
  }
}

module.exports = {
  exists,
  lock,
  unlock
}
