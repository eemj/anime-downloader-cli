const pad = str => (String(str).length === 1 ? `0${str}` : str)

const dhms = (date = new Date()) => {
  const day = pad(date.getDay())
  const hour = pad(date.getHours())
  const minute = pad(date.getMinutes())
  const seconds = pad(date.getSeconds())
  return `[${day}-${hour}:${minute}:${seconds}]`
}

let log = {}
for (const item of Object.keys(console)) {
  log[item] = (...contents) => console[item](`${dhms()}[${item.toUpperCase()}]`, ...contents)
}

module.exports = { log }
