const path = require('path')
const fs = require('fs')
const { default: scraper } = require('anime-scrape')
const progress = require('request-progress')
const request = require('request')
const { log } = require('./helpers')
const missing = require('./missing')
const lockfile = require('./lockfile')

const animePath = fs.readFileSync(path.join(__dirname, './path.txt'), { encoding: 'utf-8' }).trim()

if (!fs.existsSync(animePath)) {
  log.error(`The '${animePath}' does not exist.`)
  process.exit(1)
}

async function main() {
  lockfile.lock()

  const missingEpisodes = await missing.getMissing(animePath)

  for (const details of missingEpisodes) {
    if (details.episodes.length) {
      for (const episode of details.episodes) {
        log.info(`Found ${details.title} Episode ${episode}.`)

        const episodePath = path.join(animePath, details.link, `Episode ${episode}.mp4`)

        await new Promise(async resolve => {
          const uri = await scraper.getDownload(details.link, episode)

          log.info(`Downloading '${uri}'..`)

          if (!uri) {
            log.error(`Anime returned null '${details.title} Episode ${episode}'`)
            return resolve()
          }

          const dl = progress(request(uri))
          const stream = fs.createWriteStream(episodePath)

          let errorOccured = false

          stream.on('close', async () => {
            if (errorOccured) {
              fs.unlinkSync(episodePath)
            }
            resolve()
          })

          dl.pipe(stream)

          dl.on('error', error => {
            log.error(error)
            stream.end()
            errorOccured = true
          })

          const speeds = []

          dl.on('end', () => {
            const averageSpeeds = (speeds.length
              ? speeds.reduce((a, b) => a + b) / speeds.length
              : 0
            ).toFixed(2)

            log.info(
              `Downloaded '${
                details.title
              } Episode ${episode}' @ ${averageSpeeds}MB/s average speed.`
            )
          })

          dl.on('progress', ({ speed }) => {
            speeds.push(speed / 1024 / 1024)
          })
        })
      }
    }
  }
}

process.on('unhandledRejection', (reason, promise) => {
  log.error(`The promise '${promise}' threw an unhandled rejection reason being: ${reason}`)
  lockfile.unlock()
})

process.on('exit', () => {
  lockfile.unlock()
})

main()
  .then(() => {
    log.info('Finished')
  })
  .catch(err => {
    log.error(err)
  })
  .finally(() => {
    lockfile.unlock()
  })
